
# GitLab Docker Compose Mac

[WIP] Run GitLab, including a single runner, within Docker on a Mac, designed for easy local testing of CI/CD configuration.

Assumes:

- Docker Desktop
- DockerHub account

## Notes

To get started:

```
docker compose up
```

Received an [error with PostgreSQL timing](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/6433) so try:

(Next I copied my initial password file to a secure location.)

```
docker compose run --entrypoint /bin/bash web
```

Then:

```
gitlab-ctl reconfigure
```

(...and wait...)

It froze, seemingly forever, on a line that said:

```
ruby_block[wait for logrotate service socket] action run
```

I hit ctrl-C a couple of times to stop that nonsense, and exited the container.

According to [this issue](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/4257) we need to add some other line, but this seems deep. It ought to work.

I commented on the [PostgreSQL issue](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/6433#note_1452155852), which seems more appropriate.
